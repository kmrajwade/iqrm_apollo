#ifndef IQRM_APOLLO_IQRM_HANDLER_HPP
#define IQRM_APOLLO_IQRM_HANDLER_HPP

#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include <numeric>
#include <glob.h>
#include <string.h>
#include "iqrm_apollo/raw_bytes.hpp"
#include "iqrm_apollo/common.hpp"

// A templated binary file reader irrespective of type of data.


namespace iqrm_apollo
{

template<typename NumRepType, typename HandlerType, typename PolicyType>
class IqrmHandler
{
public:
    IqrmHandler( HandlerType& handler, PolicyType& policy, std::size_t tsamples,  std::size_t nchans, std::size_t maxlag, float nsigma, std::size_t moment); // create array based on file with this name
    ~IqrmHandler(void);

    void init(RawBytes<char>& block);


    void operator()(RawBytes<NumRepType>& block);


private:
    HandlerType& _handler;
    PolicyType& _policy;
    std::size_t _tsamples;
    std::size_t _nchans;
    std::size_t _max_lag;
    float _nsigma;
    std::size_t _moment;

};
}
#include "iqrm_apollo/detail/iqrm_handler.cpp"
#endif
