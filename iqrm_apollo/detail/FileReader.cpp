#include <iostream>
#include <sstream>
#include <iomanip>
#include "iqrm_apollo/FileReader.hpp"
#include "iqrm_apollo/sigprocheader.hpp"

// flags for opening a file that already exists
static const std::ios::openmode  	readWriteMode = std::ios::in | std::ios::out | std::ios::binary;

// flags for creating a file that does not yet exist
static const std::ios::openmode	createMode = std::ios::out | std::ios::binary;

namespace iqrm_apollo
{

 template<typename NumRepType, typename HandlerType>
FileReader<NumRepType, HandlerType>::FileReader(std::string fileName, HandlerType& handler, std::size_t nelements)
: _handler(handler)
, _nelements(nelements)
, _written_bytes(0)
, _file_size(0)
, _stop(false)
, _running(false)
{
	_file.open(fileName.c_str(), std::ios::in | std::ios::binary); // open file in read/write mode
    if(!_file)
    {
        BOOST_LOG_TRIVIAL(error) << "Error opening File";
        throw;
    }
	_status = _file.is_open() && _file.good(); // record stream status
}

 template<typename NumRepType, typename HandlerType>
FileReader<NumRepType, HandlerType>::~FileReader(void)
{
    if (!_stop)
        _stop=true;
    BOOST_LOG_TRIVIAL(info) << "Closing file";
    _file.close();
}

 template<typename NumRepType, typename HandlerType>
bool FileReader<NumRepType, HandlerType>::getStatus(void)
{
	return _status;
}

 template<typename NumRepType, typename HandlerType>
NumRepType FileReader<NumRepType, HandlerType>::getValue(int index)
{
	NumRepType	v;
	bool	eof = false;
	
	_file.seekg(index * sizeof(v));
	_file.read((char *) &v, sizeof(v));
	if (_file.eof()) {
		_file.clear(); // clear eof and fail error bits
		v = 0; // default value for Windows file system
		eof = true;
		}
	std::cout << "Value:\t" << (int) v << " <- file[" << index << "]";
	if (eof)
		std::cout << "     (eof)";
	std::cout << std::endl;
	return v;
}

// This operator[] works on the lefthand side of an assignment by
// storing values from the previous time it was called. This allows
// it to discover if it was called on the lefthand side by comparing
// the data in the 'value' data member to what it put into 'value'
// the previous time. If that has changed, then the previous call
// was on the lefthand side and the file needs to be updated.
 template<typename NumRepType, typename HandlerType>
NumRepType& FileReader<NumRepType, HandlerType>::operator[](int index)
{
	std::cout << *this;
	// write out previously set value to file if necessary
	if (_value != _prevValue)
		setValue(_prevIndex, _value);

	_value = getValue(index);

	// record current values for next time
	_prevIndex = index;
	_prevValue = _value;
	std::cout << std::endl;
	return _value;
}


template<typename NumRepType, typename HandlerType>
void FileReader<NumRepType, HandlerType>::start()
{
    bool handler_stop_request = false;
    if (_running)
    {
        throw std::runtime_error("Stream is already running");
    }
    _running=true;


    // Pass Header information
    std::vector<char> header(4096);
    SigprocHeader::read_header(_file, _header);
    auto size = SigprocHeader::write_header(header, _header);
    header.resize(size);

    /* Get Size of the File*/
    auto fsize = _file.tellg();
    _file.seekg(0, std::ios::end);
    _file_size = _file.tellg() - fsize;
    _file.seekg(_header.headersize, std::ios::beg);
    BOOST_LOG_TRIVIAL(info) << "Total file size: " << _file_size << " bytes";

    RawBytes<char> header_block(header.data(), size, 0, false);
    _handler.init(header_block);
    //Read Data now

    while(!_stop && !handler_stop_request)
    {
        
        // Get data now
	std::vector<NumRepType> data(_nelements);
        if (_file_size - _written_bytes >= _nelements)
        {
	    RawBytes<NumRepType> data_block(data.data(), _nelements,0, false);
	    handler_stop_request = read(data_block, _nelements);
	    if (handler_stop_request)
	    {
		//_handler(data_block); 
		_running = false;
		return;
	    }
	    _handler(data_block);
	}
	else if (_file_size - _written_bytes <= _nelements && _file_size - _written_bytes != 0)
        {
	    auto new_size = (_file_size - _written_bytes)/sizeof(NumRepType);
            data.resize(new_size);
            RawBytes<NumRepType> data_block(data.data(), new_size, 0, false);
            handler_stop_request = read(data_block, new_size);
	    if (handler_stop_request)
	    {
		//_handler(data_block); 
		_running = false;
		return;
	    }
	    _handler(data_block);
        }
        else
        {
	    _running=false;
            return;
        }
        
    }
    _running=false;
}

template<typename NumRepType, typename HandlerType>
void FileReader<NumRepType, HandlerType>::stop()
{
    _stop=true;
}

template<typename NumRepType, typename HandlerType>
std::streampos FileReader<NumRepType, HandlerType>::fileSize(void)
{
	_file.seekg(0, std::ios::end); // seek to end of file
	return _file.tellg(); // find out where that is
}

template<typename NumRepType, typename HandlerType>
bool FileReader<NumRepType, HandlerType>::read(RawBytes<NumRepType>& block, std::size_t& nelements)
{
    if(_status)
    {
        BOOST_LOG_TRIVIAL(info) << "Reading " << nelements << " elements of size " << sizeof(NumRepType);
        _file.read( (char *) block.ptr(), nelements * sizeof(NumRepType));

        if (_file.eof()) 
        {
            BOOST_LOG_TRIVIAL(info) << "Reached end of file!";
            return true;
        }
	_written_bytes += _nelements*sizeof(NumRepType);
        return false;
    }
    else
    {
        BOOST_LOG_TRIVIAL(error) <<  "Error on read";
        return true;
    }
}

}
