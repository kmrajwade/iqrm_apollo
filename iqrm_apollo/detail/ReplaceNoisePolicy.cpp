/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "iqrm_apollo/ReplaceNoisePolicy.hpp"
#include <algorithm>
#include <random>


namespace iqrm_apollo {


template<typename T>
ReplaceNoisePolicy<T>::ReplaceNoisePolicy()
{
}

template<typename T>
ReplaceNoisePolicy<T>::~ReplaceNoisePolicy()
{
}

template<typename T>
template<typename NumericalRep>
inline void ReplaceNoisePolicy<T>::operator()(RawBytes<NumericalRep>& block, std::size_t nchans, std::size_t tsamples, std::size_t channum, std::vector<double>& stats)
{
    std::random_device r;
    std::seed_seq seed2{r(), r(), r(), r(), r(), r(), r(), r()}; 
    std::mt19937 e2(seed2);   std::normal_distribution<float> distribution(stats[2],stats[0]);
    //BOOST_LOG_TRIVIAL(info) <<  "Median: " << stats[2] << " std dev: " << stats[0];
    for (std::size_t ii=0; ii < tsamples; ++ii)
    {
        //BOOST_LOG_TRIVIAL(info) << "Generated Noise: " << distribution(generator);
	*(reinterpret_cast<NumericalRep*>(block.ptr()) + channum + (ii * nchans)) = static_cast<NumericalRep>(distribution(e2));
    }
    
}

}

