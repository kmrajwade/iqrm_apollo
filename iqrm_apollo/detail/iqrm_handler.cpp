#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cstdio>
#include <string>
#include <cassert>
#include <new>
#include "boost/algorithm/string.hpp"
#include "iqrm_apollo/common.hpp"
#include "iqrm_apollo/raw_bytes.hpp"
#include "iqrm_apollo/iqrm_handler.hpp"

#define HANDLE_ERROR(x) \
{ \
	cudaError_t err = x; \
	if( cudaSuccess != err ) \
	{ \
		fprintf( stderr, \
		         "CUDA Error on call \"%s\": %s\n\tLine: %d, File: %s\n", \
		         #x, cudaGetErrorString( err ), __LINE__, __FILE__); \
		fflush( stdout );\
		exit( 1 ); \
	} \
}

namespace detail
{
template<typename T>
static inline double Lerp(T v0, T v1, T t)
{
    return (1 - t)*v0 + t*v1;
}

template<typename T>
static inline std::vector<T> Quantile(const std::vector<T>& inData, const std::vector<T>& probs)
{
    if (inData.empty())
    {
        return std::vector<T>();
    }

    if (1 == inData.size())
    {
        return std::vector<T>(1, inData[0]);
    }

    std::vector<T> data = inData;
    std::sort(data.begin(), data.end());
    std::vector<T> quantiles;

    for (size_t i = 0; i < probs.size(); ++i)
    {
        T poi = Lerp<T>(-0.5, data.size() - 0.5, probs[i]);

        size_t left = std::max(int64_t(std::floor(poi)), int64_t(0));
        size_t right = std::min(int64_t(std::ceil(poi)), int64_t(data.size() - 1));

        T datLeft = data.at(left);
        T datRight = data.at(right);

        T quantile = Lerp<T>(datLeft, datRight, poi - left);

        quantiles.push_back(quantile);
    }

    return quantiles;
}

class Votes
{
    public:
	Votes();
	~Votes();
	
	std::vector<std::size_t>& upvotes();   /* votes[i].upvotes() contains the list of channels that casted votes against i */

	std::vector<std::size_t>& downvotes(); /* votes[i].downvotes() contains list of channels that received a vote from i*/


    private:
	std::vector<std::size_t> _upvotes;
	std::vector<std::size_t> _downvotes;
};

Votes::Votes()
{
}

Votes::~Votes()
{
}

std::vector<std::size_t>& Votes::upvotes(){ return _upvotes; }

std::vector<std::size_t>& Votes::downvotes(){ return _downvotes; }


template<typename NumericalRepType>
void iqrmcpu_cpu_kernel(iqrm_apollo::RawBytes<NumericalRepType>& block, std::size_t tsamples, std::size_t nchans, std::vector<double>& results, std::vector<double>& means, double& median, std::size_t moment)
{

    if (moment == 1)
    {
        for (std::size_t jj=0; jj < tsamples; ++jj)
        {
            for (std::size_t ii=0; ii < nchans; ++ii)
            {
                results[ii] += ((double)(*(reinterpret_cast<NumericalRepType*>(block.ptr() + ii + jj*nchans))) * (double)(*(reinterpret_cast<NumericalRepType*>(block.ptr()+ ii + jj*nchans)))/(double)(tsamples));
                means[ii] += ((double)(*(reinterpret_cast<NumericalRepType*>(block.ptr() + ii + jj*nchans)))/(double)tsamples);
            }

        }
        for (std::size_t ii=0; ii < nchans; ++ii)
        {
            results[ii] = std::sqrt(results[ii] - (means[ii] * means[ii]));
        }
    }
    else if (moment == 3)
    {
        std::vector<double> mean_cube(means.size(),0.0);
        for (std::size_t jj=0; jj < tsamples; ++jj)
        {
            for (std::size_t ii=0; ii < nchans; ++ii)
            {
                results[ii] += ((double)(*(reinterpret_cast<NumericalRepType*>(block.ptr() + ii + jj*nchans))) * (double)(*(reinterpret_cast<NumericalRepType*>(block.ptr()+ ii + jj*nchans)))/(double)(tsamples));
                means[ii] += ((double)(*(reinterpret_cast<NumericalRepType*>(block.ptr() + ii + jj*nchans)))/(double)tsamples);
                mean_cube[ii] += (powf((double)(*(reinterpret_cast<NumericalRepType*>(block.ptr() + ii + jj*nchans))), 3)/(double)tsamples);
            }

        }
        for (std::size_t ii=0; ii < nchans; ++ii)
        {
            double std_dev = std::sqrt(results[ii] - (means[ii] * means[ii]));
            results[ii] = std::abs((mean_cube[ii] - 3.0 * (means[ii] * std_dev * std_dev ) - powf(means[ii], 3))/powf(std_dev, 3));
        }
    }
    else if (moment == 4)
    {
        std::vector<double> variance_squared(means.size(),0.0);
        for (std::size_t jj=0; jj < tsamples; ++jj)
        {
            for (std::size_t ii=0; ii < nchans; ++ii)
            {
			    means[ii] += ((double)(*(reinterpret_cast<NumericalRepType*>(block.ptr() + ii + jj*nchans)))/(double)tsamples);
			    results[ii] += ((double)(*(reinterpret_cast<NumericalRepType*>(block.ptr() + ii + jj*nchans))) * (double)(*(reinterpret_cast<NumericalRepType*>(block.ptr()+ ii + jj*nchans)))/(double)(tsamples));
		    }
		    for (std::size_t ii=0; ii < nchans; ++ii)
		    {
			    variance_squared[ii] += (powf((double)(*(reinterpret_cast<NumericalRepType*>(block.ptr() + ii + jj*nchans))) - means[ii], 4)/(double)tsamples);
		    }
	    }
	    for (std::size_t ii=0; ii < nchans; ++ii)
	    {
		    double std_dev = std::sqrt(results[ii] - (means[ii] * means[ii]));
		    results[ii] = variance_squared[ii]/powf(std_dev, 4);
	    }
    }
    else if (moment == 5)
    {
	   std::vector<double> acf(means.size(), 0.0);
       //std::vector<double> acf_sum(means.size(), 0.0);
       for (std::size_t jj=0; jj < tsamples; ++jj)
	   {
		    for (std::size_t ii=0; ii < nchans; ++ii)
            {
                results[ii] += ((double)(*(reinterpret_cast<NumericalRepType*>(block.ptr() + ii + jj*nchans))) * (double)(*(reinterpret_cast<NumericalRepType*>(block.ptr()+ ii + jj*nchans)))/(double)(tsamples));

                means[ii] += ((double)(*(reinterpret_cast<NumericalRepType*>(block.ptr() + ii + jj*nchans)))/(double)tsamples);

                if (jj < tsamples -1)
                {
                    acf[ii] += ((double)(*(reinterpret_cast<NumericalRepType*>(block.ptr() + ii + jj*nchans))) * (double)(*reinterpret_cast<NumericalRepType*>(block.ptr() + ii + (jj+1)*nchans)));

                    //acf_sum[ii] +=((double)(*(reinterpret_cast<NumericalRepType*>(block.ptr() + ii + jj*nchans))) + (double)(*reinterpret_cast<NumericalRepType*>(block.ptr() + ii + (jj+1)*nchans)));
                }
            }

       }

       for (std::size_t ii =0; ii < nchans; ++ii)
       {
           double var = std::abs(results[ii] - (means[ii] * means[ii]));
           results[ii] = std::abs((acf[ii]/(tsamples-1)) - means[ii]*means[ii])/var;
       }
    }
    median = detail::Quantile<double>(means, {0.25,0.5,0.75})[1];
}

}

using namespace iqrm_apollo;

template<typename NumRepType, typename HandlerType, typename PolicyType>
IqrmHandler<NumRepType, HandlerType, PolicyType>::IqrmHandler(HandlerType& handler, PolicyType& policy, std::size_t tsamples, std::size_t nchans, std::size_t maxlags, float nsigma, std::size_t moment)
: _handler(handler)
, _policy(policy)
, _tsamples(tsamples)
, _nchans(nchans)
, _max_lag(maxlags)
, _nsigma(nsigma)
, _moment(moment)
{
    if (_moment != 1 && _moment != 3 && _moment != 4 && _moment != 5)
	throw "Wrong moment parsed. It can either be 1 (std dev) or 3(skewness) or 4(kurtosis) or 5(acf)!";
}

template<typename NumRepType, typename HandlerType, typename PolicyType>
IqrmHandler<NumRepType, HandlerType, PolicyType>::~IqrmHandler()
{
}

template<typename NumRepType, typename HandlerType, typename PolicyType>
void IqrmHandler<NumRepType, HandlerType, PolicyType>::init(RawBytes<char>& block)
{
    _handler.init(block);
}

template<typename NumRepType, typename HandlerType, typename PolicyType>
void IqrmHandler<NumRepType, HandlerType, PolicyType>::operator()(RawBytes<NumRepType>& block)
{
    try
    {
	    // Allocate host memory
	    std::vector<double> results(_nchans,0);
	    std::vector<double> means(_nchans,0);
        std::vector<double> stats;
        double median;

	    BOOST_LOG_TRIVIAL(info) << "Running IQRM Algorithm....";

	    // check for smaller number of samples in the last block
	    if (block.total_bytes()/sizeof(NumRepType) != _tsamples*_nchans)
            _tsamples = block.total_bytes()/sizeof(NumRepType)/_nchans;
	    // compute per channel std for the TF block
	    detail::iqrmcpu_cpu_kernel<NumRepType>(block, _tsamples, _nchans, results, means, median, _moment);

	    // Compute IQR range
	    std::vector<bool> flags(results.size(), false);
	    std::vector<double> diff_vector(results.size());
	    std::vector<bool> flags_per_lag(results.size(), false);
	    std::vector<detail::Votes> votes(results.size());
	    //std::vector<double> original_data = results;

	    std::int32_t lag_progression=0;
        std::vector<int32_t> positive_lags;
	    std::vector<int32_t> lags;
	    while (lag_progression < static_cast<int32_t>(_max_lag))
        {
            lag_progression = std::max((int32_t)(1.5 * lag_progression), lag_progression + 1);
            positive_lags.push_back(lag_progression);
            lags.push_back(-lag_progression);
	    }
	    std::reverse(lags.begin(), lags.end());
            lags.insert(lags.end(), positive_lags.begin(), positive_lags.end());

	    //for (std::int32_t ii= -1*static_cast<int32_t>(_max_lag); ii<= static_cast<int32_t>(_max_lag); ii = ii + lag_progression)
	    for (std::int32_t mm = 1; mm< lags.size()-1; ++mm)
	    {
		    auto ii = lags[mm];
		    //Rotate the vector
		    std::size_t jj = 0;

		    for (std::size_t kk=0; kk < results.size(); ++kk)
		    {
			    if (ii < 0 && kk < results.size() + ii )
				diff_vector[kk] = results[kk] - results[kk - ii];
			    else if ( ii < 0 && kk >= results.size() + ii)    /* Clipping at the end of the array to the same value*/
				diff_vector[kk] = results[results.size() + ii] - results[_nchans - 1];

			    if (ii > 0 && kk < ii)   /* Clipping at the start of the array to the same value */
				diff_vector[kk] = results[ii] - results[0];
			    else if ( ii > 0 && kk >= ii)
				diff_vector[kk] = results[kk] - results[kk - ii];
		    }

		    std::vector<double> quant = detail::Quantile<double>(diff_vector, {0.25, 0.5, 0.75});
            BOOST_LOG_TRIVIAL(info) << "Quantiles are: " << quant[0] << " " << quant[1] << " " << quant[2];
            // Generate a difference vector for different lags
            double std_dev = std::abs((quant[2] - quant[0]) / 1.349);

                    // populate the stats vector for replacement policy

            stats.push_back((double)(std::accumulate(results.begin(),results.end(),0))/(double)results.size());
            stats.push_back((double)(std::accumulate(means.begin(),means.end(),0))/(double)means.size());
            stats.push_back(median);

            std::generate(flags_per_lag.begin(), flags_per_lag.end(), [&] () {bool flag = diff_vector[jj] - quant[1] > _nsigma*std_dev;
                    ++jj;
                    return flag; });


            //Create a list of upvotes and downvotes for every lag for a given channel
            for (std::size_t kk=0; kk < flags_per_lag.size(); ++kk)
            {

                if (flags_per_lag[kk] == true)
                {
                    if (ii < 0 && kk < results.size() + ii)
                    {
                        votes[kk].upvotes().push_back(kk - ii);
                        votes[kk - ii].downvotes().push_back(kk);
                    }
                    else if ( ii < 0 && kk >= results.size() + ii)
                    {
                        votes[kk].upvotes().push_back(_nchans - 1);
                        votes[_nchans - 1].downvotes().push_back(kk);
                    }
                    else if ( ii > 0  && kk < ii)
                    {
                        votes[kk].upvotes().push_back(0);
                        votes[0].downvotes().push_back(kk);
                    }
                    else if ( ii > 0 && kk >= ii)
                    {
                        votes[kk].upvotes().push_back(kk - ii);
                        votes[kk - ii].downvotes().push_back(kk);
                    }
                }
		    }
		    /*if ( loop_num > 1)
		        ++lag_progression;

		    ++ loop_num;*/

	    }
	    // Generate new flags based on the voting of channels
	    double fraction = 0.0;
	    for (std::size_t ii=0; ii < votes.size(); ++ii)
	    {
            detail::Votes channel = votes[ii];
            for (std::size_t jj=0; jj < channel.upvotes().size(); ++jj)
            {
                if (votes[channel.upvotes()[jj]].downvotes().size() < channel.upvotes().size() )
                {
                    flags[ii] = true;
                    _policy(block, _nchans, _tsamples, ii, stats);
                    ++fraction;
                    break;
                }
            }

        }

        BOOST_LOG_TRIVIAL(info) << "Fraction of channels flagged: " << (fraction/(double)_nchans);
        BOOST_LOG_TRIVIAL(info) << "IQRM finished";
    }

    catch (...)
    {
        throw;
    }

    _handler(block);
}

