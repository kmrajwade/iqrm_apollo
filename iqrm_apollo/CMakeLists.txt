# define the cheetah libraries
set(IQRM_APOLLO_CPP_LIBRARIES ${CMAKE_PROJECT_NAME} ${DEPENDENCY_LIBRARIES})

include_directories(..)
include_directories(${PROJECT_BINARY_DIR})

set(iqrm_apollo_src
    src/simple_file_writer.cpp
    src/sigprocheader.cpp
   )

set(iqrm_apollo_inc
  raw_bytes.hpp
  simple_file_writer.hpp
  sigprocheader.hpp
  )

# -- the main library target
add_library(${CMAKE_PROJECT_NAME} ${iqrm_apollo_src})

#tb_trans
add_executable(iqrm_apollo_cli src/iqrm_cli.cpp)
target_link_libraries (iqrm_apollo_cli ${IQRM_APOLLO_CPP_LIBRARIES})

install(FILES ${iqrm_apollo_inc} DESTINATION include/iqrm_apollo)
