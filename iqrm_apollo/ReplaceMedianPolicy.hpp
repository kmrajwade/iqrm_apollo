/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef IQRM_APOLLO_REPLACEMEDIANPOLICY_H
#define IQRM_APOLLO_REPLACEMEDIANPOLICY_H

#include "iqrm_apollo/raw_bytes.hpp"

namespace iqrm_apollo {

/**
 * @brief
 * @details
 */

template<typename T>
class ReplaceMedianPolicy
{
    public:
        ReplaceMedianPolicy();
        ~ReplaceMedianPolicy();

        /// replace all channels in the chunks with a constant value

        template<typename NumericalRep>
        void operator()(RawBytes<NumericalRep>& block, std::size_t nchans, std::size_t tsamples, std::size_t channum, std::vector<double>& stats);

};


} // namespace channel_mask
#include "iqrm_apollo/detail/ReplaceMedianPolicy.cpp"

#endif // IQRM_APOLLO_REPLACECONSTANTPOLICY_H
